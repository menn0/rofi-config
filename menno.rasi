/**
 * rofi -dump-theme output.
 * Rofi version: 1.7.3
 **/
* {
    foreground:                  #cfcfcf;
    background-color:            transparent;
    background:                  #173f5f;

    foreground-alt:              #3a3a3a;
    background-alt:              #3caea3;

    red:                         #F6D55C;
    yellow:                      #20639B; 

    normal-background:           var(background);
    normal-foreground:           var(foreground);
    active-background:           var(background-alt);
    active-foreground:           var(foreground-alt);
    alternate-active-background: var(background-alt);
    alternate-active-foreground: var(foreground-alt);
    alternate-normal-background: var(background);
    alternate-normal-foreground: var(foreground);
    alternate-urgent-background: var(red);
    alternate-urgent-foreground: var(foreground-alt);
    border-color:                var(foreground);
    selected-active-background:  var(background-alt);
    selected-active-foreground:  var(foreground-alt);
    selected-normal-background:  var(background-alt);
    selected-normal-foreground:  var(foreground-alt);
    selected-urgent-background:  var(yellow);
    selected-urgent-foreground:  var(foreground-alt);
    separatorcolor:              var(foreground);
    spacing:                     2;
    urgent-background:           var(red);
    urgent-foreground:           var(foreground-alt);
}
element {
    padding: 3px ;
    cursor:  pointer;
    spacing: 5px ;
    border:  0;
}
element normal.normal {
    background-color: var(normal-background);
    text-color:       var(normal-foreground);
}
element normal.urgent {
    background-color: var(urgent-background);
    text-color:       var(urgent-foreground);
}
element normal.active {
    background-color: var(active-background);
    text-color:       var(active-foreground);
}
element selected.normal {
    background-color: var(selected-normal-background);
    text-color:       var(selected-normal-foreground);
}
element selected.urgent {
    background-color: var(selected-urgent-background);
    text-color:       var(selected-urgent-foreground);
}
element selected.active {
    background-color: var(selected-active-background);
    text-color:       var(selected-active-foreground);
}
element alternate.normal {
    background-color: var(alternate-normal-background);
    text-color:       var(alternate-normal-foreground);
}
element alternate.urgent {
    background-color: var(alternate-urgent-background);
    text-color:       var(alternate-urgent-foreground);
}
element alternate.active {
    background-color: var(alternate-active-background);
    text-color:       var(alternate-active-foreground);
}
element-text {
    background-color: transparent;
    cursor:           inherit;
    highlight:        inherit;
    text-color:       inherit;
}
element-icon {
    background-color: transparent;
    size:             1.0000em ;
    cursor:           inherit;
    text-color:       inherit;
}
window {
    padding:          5;
    background-color: var(background);
    border:           0;
}
mainbox {
    padding: 8px;
    border:  0;
}
message {
    padding:      1px ;
    border-color: var(separatorcolor);
    border:       2px dash 0px 0px ;
}
textbox {
    text-color: var(foreground);
}
listview {
    padding:      2px 0px 0px ;
    scrollbar:    true;
    border-color: var(separatorcolor);
    spacing:      2px ;
    fixed-height: 0;
    border:       2px dash 0px 0px ;
}
scrollbar {
    width:        4px ;
    padding:      0;
    handle-width: 8px ;
    border:       0;
    handle-color: var(normal-foreground);
}
sidebar {
    border-color: var(separatorcolor);
    border:       2px dash 0px 0px ;
}
button {
    cursor:     pointer;
    spacing:    0;
    text-color: var(normal-foreground);
}
button selected {
    background-color: var(selected-normal-background);
    text-color:       var(selected-normal-foreground);
}
num-filtered-rows {
    expand:     false;
    text-color: Gray;
}
num-rows {
    expand:     false;
    text-color: Gray;
}
textbox-num-sep {
    expand:     false;
    str:        "/";
    text-color: Gray;
}
inputbar {
    padding:    1px ;
    spacing:    0px ;
    text-color: var(normal-foreground);
    children:   [ "prompt","textbox-prompt-colon","entry","num-filtered-rows","textbox-num-sep","num-rows","case-indicator" ];
}
case-indicator {
    spacing:    0;
    text-color: var(normal-foreground);
}
entry {
    text-color:        var(normal-foreground);
    cursor:            text;
    spacing:           0;
    placeholder-color: Gray;
    placeholder:       "Type to filter";
}
prompt {
    spacing:    0;
    text-color: var(normal-foreground);
}
textbox-prompt-colon {
    margin:     0px 0.3000em 0.0000em 0.0000em ;
    expand:     false;
    str:        ":";
    text-color: inherit;
}
